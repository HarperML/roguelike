﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

    public class GameController : MonoBehaviour {
	 
    public static GameController Instance;
    public bool isPlayerTurn;
    public bool areEnemiesMoving;

	private BoardController boardController;
    private List<Enemy> enemies;
    private GameObject levelImage;
    private Text levelText;
    private bool settingUpGame;
    private int secondsUntilLevelstart = 2;
    private int currentLevel = 1;

	void Awake () {
		if(Instance != null && Instance !=this)
		{
			Destroy(gameObject);
			return;
	}

		Instance = this;
		DontDestroyOnLoad (gameObject); 
		boardController = GetComponent<BoardController>();
        enemies = new List<Enemy>();
	}

	void Start()
	{
	    boardController.SetupLevel();
        isPlayerTurn = true;
        areEnemiesMoving = false;
	}

	void Update () {
        if(isPlayerTurn || areEnemiesMoving)
        {
            return;
        }

    private void IntializeGame()
    {
        settingUpGame = true;
        levelImage = GameObject.Find("Level image");
        levelText = GameObject.Find("Level Text").GetComponent<Text>();
        levelText.text = "Day" + currentLevel;
        levelImage.SetActive(true);
        enemies.Clear();
        boardController.SetupLevel(currentLevel);
        invoke("DisableLevelImage", secondsUntilLevelstart);
    }
	   
    private void DisableLevelImage()
    {
        levelImage.SetActive(false);
        settingUpGame = false;
        isPlayerTurn = true;
        areEnemiesMoving = false;
    }

    private void OnLevelWasLoaded(int levelLoaded)
    {
        currentLevel++;
        IntializeGame();
	}

    private IEnumerator MoveEnemies()
    {
        areEnemiesMoving = true;

        yield return new WaitForSeconds(0.2f);

        foreach(Enemy enemy in enemies)
        {
            enemy.MoveEnemy();
            yield return new WaitForSeconds(enemy.moveTime);
        }


        areEnemiesMoving = false;
        isPlayerTurn = true;
    }

    public void AddEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }
}
